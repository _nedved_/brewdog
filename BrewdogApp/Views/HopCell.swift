//
//  Created by Andrei Niadzvedtski on 22/06/2020.
//  Copyright © 2020 Andrei Niadzvedtski. All rights reserved.
//

import UIKit

class HopCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var additionalInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(with hop: Hop){
        name.text = "Name: " + hop.name
        amount.text = "Amount: \(hop.amount.value) \(hop.amount.unit)"
        additionalInfo.text = "Additional Info: " + hop.add + " " + hop.attribute
    }

}
