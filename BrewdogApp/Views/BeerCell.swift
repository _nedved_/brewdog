//
//  Created by Andrei Niadzvedtski on 21/06/2020.
//  Copyright © 2020 Andrei Niadzvedtski. All rights reserved.
//

import UIKit
import RxSwift

class BeerCell: UITableViewCell {
    static let cellId = "BeerCellIdentifier"
    
    private let disposeBag = DisposeBag()
    private var viewModel: CellViewModel!

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var abv: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        activityIndicator.startAnimating()
        imgView.image = nil
        name.text = ""
        abv.text = ""
    }

    func setup(with viewModel: CellViewModel) {
        self.viewModel = viewModel
        name.text = viewModel.beer.name
        abv.text = "ABV: \(viewModel.beer.abv)"
    
        viewModel.isCurrentlyFetching.map{!$0}.bind(to: activityIndicator.rx.isHidden).disposed(by: disposeBag)
        viewModel.image.bind(to: imgView.rx.image).disposed(by: disposeBag)
    }

}
