//
//  Created by Andrei Niadzvedtski on 22/06/2020.
//  Copyright © 2020 Andrei Niadzvedtski. All rights reserved.
//

import UIKit

class MaltCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var amount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(with malt: Malt){
        name.text = malt.name
        amount.text = "\(malt.amount.value) \(malt.amount.unit)"
    }

}
