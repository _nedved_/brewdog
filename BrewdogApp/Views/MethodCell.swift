//
//  Created by Andrei Niadzvedtski on 22/06/2020.
//  Copyright © 2020 Andrei Niadzvedtski. All rights reserved.
//

import UIKit

class MethodCell: UITableViewCell {
    static let cellId = "MethodCellIdentifier"
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var duration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        temperature.text = ""
        duration.text = ""
    }
    
    func setup(with mashTemp: MashTemp) {
        temperature.text = "Temperature: " + (mashTemp.temp.value != nil ? "\(mashTemp.temp.value!)" : "--") + " \(mashTemp.temp.unit)"
        duration.text = "Duration: " + (mashTemp.duration != nil ? "\(mashTemp.duration!)" : "--")
    }

}
