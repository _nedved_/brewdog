//
//  Created by Andrei Niadzvedtski on 22/06/2020.
//  Copyright © 2020 Andrei Niadzvedtski. All rights reserved.
//

import UIKit

class TextCell: UITableViewCell {
    static let cellId = "TextCellIdentifier"
    @IBOutlet weak var txt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(with text: String) {
        self.txt.text = text
    }

}
