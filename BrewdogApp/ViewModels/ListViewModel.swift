//
//  Created by Andrei Niadzvedtski on 20/06/2020.
//  Copyright © 2020 Andrei Niadzvedtski. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

class ListViewModel {
    private let disposeBag = DisposeBag()
    private let beersProvider: BeersProvider

    var startResultsFetching = BehaviorRelay<Bool>.init(value: false)
    var items = BehaviorRelay<[Beer]>(value: [Beer]())


    init(beersProvider: BeersProvider) {
        self.beersProvider = beersProvider
        setUpSubscriptions()
    }

    var isCurrentlyFetching = BehaviorRelay<Bool>.init(value: false)
    func fetchResults() {
        guard isCurrentlyFetching.value == false else { return }

        isCurrentlyFetching.accept(true)
        beersProvider.fetchResults().subscribe(onNext: { [weak self] fetchResult in
            guard let self = self else { return }

            self.isCurrentlyFetching.accept(false)

            switch fetchResult {
            case .success(let value):
                let result = self.items.value + value
                self.items.accept(result)
            case .failure(let error):
                debugPrint("\(error)")
            }
        }).disposed(by: disposeBag)
    }

    private func setUpSubscriptions() {
        startResultsFetching.filter{$0}.subscribe({ [weak self] _ in
            guard let self = self else { return }
            self.fetchResults()
        }).disposed(by: disposeBag)
    }

}
