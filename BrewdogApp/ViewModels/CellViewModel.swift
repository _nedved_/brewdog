//
// Created by Andrei Niadzvedtski on 21/06/2020.
// Copyright (c) 2020 Andrei Niadzvedtski. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxRelay


class CellViewModel {
    private let disposeBag = DisposeBag()
    private let apiFetcher: FetchProtocol

    let beer: Beer
    let isCurrentlyFetching = BehaviorRelay<Bool>.init(value: false)
    let image = PublishSubject<UIImage>()

    init(beer: Beer, apiFetcher: FetchProtocol) {
        self.beer = beer
        self.apiFetcher = apiFetcher
        startFetching()
    }

    private func startFetching() {
        guard let url = beer.image_url else { return }
        
        self.isCurrentlyFetching.accept(true)
        apiFetcher.fetchData(with: url).subscribe(onNext:  { [unowned self] fetchResult in
            self.isCurrentlyFetching.accept(false)
            
            switch fetchResult {
            case .success(let value):
                guard let image = UIImage(data: value) else { return }
                self.image.onNext(image)
            case .failure(let value):
                debugPrint("\(value)")
            }
        }).disposed(by: disposeBag)
    }
}
