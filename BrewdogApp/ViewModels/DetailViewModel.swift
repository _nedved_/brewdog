//
//  Created by Andrei Niadzvedtski on 20/06/2020.
//  Copyright © 2020 Andrei Niadzvedtski. All rights reserved.
//

import Foundation
import UIKit
import RxAlamofire
import RxSwift
import RxRelay

class DetailViewModel {
    private let disposeBag = DisposeBag()
    private let apiFetcher: FetchProtocol
    
    let image = PublishSubject<UIImage>()
    let isCurrentlyFetching = BehaviorRelay<Bool>.init(value: false)
    
    var beer: Beer

    init(with beer: Beer, apiFetcher: FetchProtocol) {
        self.beer = beer
        self.apiFetcher = apiFetcher
        fetchImage()
    }
    
    private func fetchImage() {
        guard let url = beer.image_url else { return }
        self.isCurrentlyFetching.accept(true)
        apiFetcher.fetchData(with: url).subscribe(onNext:  { [unowned self] fetchResult in
            self.isCurrentlyFetching.accept(false)
            
            switch fetchResult {
            case .success(let value):
                guard let image = UIImage(data: value) else { return }
                self.image.onNext(image)
            case .failure(let value):
                debugPrint("\(value)")
            }
        }).disposed(by: disposeBag)
    }
}
