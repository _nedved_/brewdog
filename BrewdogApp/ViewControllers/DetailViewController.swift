//
// Created by Andrei Niadzvedtski on 21/06/2020.
// Copyright (c) 2020 Andrei Niadzvedtski. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources


class DetailViewController: UIViewController {
    static let Identifier = "DetailViewController"
    
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var abv: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ph: UILabel!
    @IBOutlet weak var ebc: UILabel!
    @IBOutlet weak var srm: UILabel!
    @IBOutlet weak var ibu: UILabel!
    
    var viewModel: DetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignTitle()
        setupLabels()
        setupActivityIndicator()
        setupImageView()
        setupTableView()
    }

    private func setupLabels() {
        let model = viewModel.beer

        abv.text = "ABV: \(model.abv)"
        ph.text = "ph: " + (model.ph != nil ? "\(model.ph!)" : "--")
        ebc.text = "ebc: " + (model.ebc != nil ? "\(model.ebc!)" : "--")
        srm.text = "srm: " + (model.srm != nil ? "\(model.srm!)" : "--")
        ibu.text = "ibu:  " + (model.ibu != nil ? "\(model.ibu!)" : "--")
    }

    private func assignTitle() {
        let model = viewModel.beer
        title = model.name
    }

    private func setupImageView() {
        viewModel.image.bind(to: imgView.rx.image).disposed(by: disposeBag)
    }

    private func setupActivityIndicator() {
        viewModel.isCurrentlyFetching.map{!$0}.bind(to: activityIndicator.rx.isHidden).disposed(by: disposeBag)
    }


    private func setupTableView() {



        let dataSource = RxTableViewSectionedReloadDataSource<Section>(configureCell: { section, tableView, indexPath, item -> UITableViewCell in
            switch section[indexPath] {
            case let .HopItem(hop):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "HopCell", for: indexPath) as? HopCell else { return UITableViewCell() }
                cell.setup(with: hop)
                return cell
            case .MaltItem(let malt):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MaltCell", for: indexPath) as? MaltCell else { return UITableViewCell() }
                cell.setup(with: malt)
                return cell
            case .MethodItem(let mashTemp):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MethodCellIdentifier", for: indexPath) as? MethodCell else { return UITableViewCell() }
                cell.setup(with: mashTemp)
                return cell
            case .TextItem(let text):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "TextCellIdentifier", for: indexPath) as? TextCell else { return UITableViewCell() }
                cell.setup(with: text)
                return cell
            }
        }, titleForHeaderInSection: { sectons, index in
            let section = sectons[index]
            return section.title
        }
        )

        let model = viewModel.beer

        let hops = model.ingredients.hops
        let malts = model.ingredients.malts
        let mashTemps = model.method.mash_temp
        let fermentation = model.method.fermentation
        let fermentationText = (fermentation.temp.value != nil ? "\(fermentation.temp.value!)" : "--") + " \(fermentation.temp.unit)"
        let twist = model.method.twist
        let desc = model.description
        
        let hopItems: [SectionItem] = hops.map { SectionItem.HopItem(hop: $0) }
        let maltsItems: [SectionItem] = malts.map { SectionItem.MaltItem(malt: $0) }
        let mashTempItems: [SectionItem] = mashTemps.map { SectionItem.MethodItem(method: $0)}
        let fermentationItems: [SectionItem] = [SectionItem.TextItem(text: fermentationText)]
        let twistItems: [SectionItem] = [SectionItem.TextItem(text: twist ?? "--")]
        let descriptionItems: [SectionItem] = [SectionItem.TextItem(text: desc)]
        
        
        let sections: [Section] = [
            .TextSection(title: "Description", items: descriptionItems),
            .HopSection(title: "Hops", items: hopItems),
            .MaltSection(title: "Malts", items: maltsItems),
            .MethodSection(title: "Method", items: mashTempItems),
            .TextSection(title: "Fermentation", items: fermentationItems),
            .TextSection(title: "Twist", items: twistItems)
        ]
        
        Observable.just(sections).bind(to: tableView.rx.items(dataSource: dataSource)).disposed(by: disposeBag)
    }
}

enum Section {
    case HopSection(title: String, items: [SectionItem])
    case MaltSection(title: String, items: [SectionItem])
    case MethodSection(title: String, items: [SectionItem])
    case TextSection(title: String, items: [SectionItem])
}


enum SectionItem {
    case HopItem(hop: Hop)
    case MaltItem(malt: Malt)
    case MethodItem(method: MashTemp)
    case TextItem(text: String)
}

extension Section: SectionModelType {
    typealias Item = SectionItem
    
    init(original: Section, items: [SectionItem]) {
        switch original {
        case .HopSection(let title, let items):
            self = .HopSection(title: title, items: items)
        case .MaltSection(let title, let items):
            self = .HopSection(title: title, items: items)
        case .MethodSection(let title, let items):
            self = .MethodSection(title: title, items: items)
        case .TextSection(let title, let items):
            self = .TextSection(title: title, items: items)
            
        }
    }
    
    var items: [SectionItem] {
        switch self {
        case .HopSection(title: _, items: let items):
            return items
        case .MaltSection(title: _, items: let items):
            return items
        case .MethodSection(title: _, items: let items):
            return items
        case .TextSection(title: _, items: let items):
            return items
        }
    }
    
    var title: String {
        switch self {
        case  .HopSection(title: let title, items: _):
            return title
        case .MaltSection(title: let title, items: _):
            return title
        case .MethodSection(title: let title, items: _):
            return title
        case .TextSection(title: let title, items: _):
            return title
        }
    }
}
