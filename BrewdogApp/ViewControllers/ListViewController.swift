//
//  Created by Andrei Niadzvedtski on 20/06/2020.
//  Copyright © 2020 Andrei Niadzvedtski. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ListViewController: UIViewController {
    private let disposeBag = DisposeBag()
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel: ListViewModel = {
        let request = BeersRequest(page: 1, resultsPerPage: 30)
        let provider = BeersProvider(with: request, fetcher: ApiFetcher.shared)
        let result = ListViewModel(beersProvider: provider)
        
        return result
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.startResultsFetching.accept(true)
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.rx.itemSelected
            .subscribe({ [unowned self] indexPath in
                
                guard let indexPath = indexPath.element else { return }
                
                let beer = self.viewModel.items.value[indexPath.row]
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: DetailViewController.Identifier) as? DetailViewController else { return }
                vc.viewModel = DetailViewModel(with: beer, apiFetcher: ApiFetcher.shared)
                self.navigationController?.pushViewController(vc, animated: true)
                self.tableView.deselectRow(at: indexPath, animated: true)
            }).disposed(by: disposeBag)
        
        viewModel.items
            .bind(to: tableView.rx.items(cellIdentifier: BeerCell.cellId, cellType: BeerCell.self)) { (row, element, cell) in
                let cellViewModel = CellViewModel(beer: element, apiFetcher: ApiFetcher.shared)
                cell.setup(with: cellViewModel)
        }.disposed(by: disposeBag)
        
        
        tableView.rx
            .contentOffset.subscribe(onNext: { [unowned self] contentOffset in
                guard contentOffset != .zero else { return }
                guard contentOffset.y + self.tableView.frame.size.height > self.tableView.contentSize.height else { return }
                
                self.viewModel.fetchResults()
            }).disposed(by: disposeBag)
    }
}

