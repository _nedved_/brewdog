//
// Created by Andrei Niadzvedtski on 21/06/2020.
// Copyright (c) 2020 Andrei Niadzvedtski. All rights reserved.
//

import Foundation
import RxAlamofire
import Alamofire

class Request<Result: Codable> {
    var httpMethod: HTTPMethod = .get
    var url: String!

    init() {
        
    }

    func decode(_ data: Data) throws -> Result {
        try JSONDecoder().decode(Result.self, from: data)
    }
    
    func parameters() -> [String: Any] {
        return [String: Any]()
    }
}
