//
// Created by Andrei Niadzvedtski on 21/06/2020.
// Copyright (c) 2020 Andrei Niadzvedtski. All rights reserved.
//

import Foundation

class BeersRequest : Request<[Beer]> {
    var page: Int
    var perPage: Int

    init(page: Int, resultsPerPage: Int) {
        self.page = page
        self.perPage = resultsPerPage
        super.init()
        self.url = "https://api.punkapi.com/v2/beers"
    }

    override func parameters() -> [String: Any] {
        ["page" : page, "per_page": perPage]
    }
}
