//
// Created by Andrei Niadzvedtski on 21/06/2020.
// Copyright (c) 2020 Andrei Niadzvedtski. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import RxAlamofire


enum FetchResult<Success, Failure> {
    case success(Success)
    case failure(Failure)

    public var success: Success? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }

    public var failure: Failure? {
        switch self {
        case .success:
            return nil
        case .failure(let value):
            return value
        }
    }
}

enum FetchError: Error {
    case dataParseError
    case urlCreationError
    case requestDataError
}

protocol FetchProtocol {
    func fetchData(with urlString: String) -> Observable<FetchResult<Data, FetchError>>
    func fetch<Result>(with request: Request<Result>) -> Observable<FetchResult<Result, FetchError>>
}

class ApiFetcher: FetchProtocol {
    static let shared = ApiFetcher()
    private init() {

    }

    func fetchData(with urlString: String) -> Observable<FetchResult<Data, FetchError>> {
        let session = Session.default
        
        guard let url = URL(string: urlString) else { return Observable.just(FetchResult<Data, FetchError>.failure(.urlCreationError))}
        
        let urlRequest = URLRequest(url: url)
        
        let result = session.rx.request(urlRequest: urlRequest)
            .data()
            .single()
            .map { FetchResult.success($0) }
            .catchError{ err in
                return Observable.just(FetchResult<Data, FetchError>.failure(.requestDataError))
        }
        
        return result
    }

    func fetch<Result>(with request: Request<Result>) -> Observable<FetchResult<Result, FetchError>> {
        let session = Session.default
        let result = session.rx.request(request.httpMethod, request.url, parameters: request.parameters())
                .responseData()
                .flatMapLatest({ (response, data) -> Observable<FetchResult<Result, FetchError>> in
                    do {
                        let result = try request.decode(data)
                        return Observable.just(FetchResult<Result, FetchError>.success(result)
                        )
                    } catch {
                        debugPrint("decoding error: \(error)")
                    }
                    return Observable.just(FetchResult<Result, FetchError>.failure(.dataParseError))
                })

        return result
    }
}
