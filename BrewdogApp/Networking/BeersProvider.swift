//
// Created by Andrei Niadzvedtski on 21/06/2020.
// Copyright (c) 2020 Andrei Niadzvedtski. All rights reserved.
//

import Foundation
import RxSwift

class BeersProvider {
    private let fetcher: FetchProtocol
    private let request: BeersRequest

    init(with request: BeersRequest, fetcher: FetchProtocol) {
        self.request = request
        self.fetcher = fetcher
    }

    func fetchResults() -> Observable<FetchResult<[Beer], FetchError>> {
        return fetcher.fetch(with: request).flatMapLatest { [unowned self] fetchResult -> Observable<FetchResult<[Beer], FetchError>> in
            switch fetchResult {
            case .success:
                self.request.page += 1
            case .failure: break
            }

            return Observable.just(fetchResult)
        }
    }
}
