//
// Created by Andrei Niadzvedtski on 21/06/2020.
// Copyright (c) 2020 Andrei Niadzvedtski. All rights reserved.
//

import Foundation


struct Temp: Codable {
    var value: Int?
    var unit: String
}

struct MashTemp: Codable {
    var temp: Temp
    var duration: Int?
}

struct Fermentation: Codable {
    var temp: Temp
}

struct Hop: Codable  {
    var name, add, attribute: String
    var amount: Amount
}

struct Amount: Codable {
    var value: Float
    var unit: String
}

struct Malt: Codable {
    var name: String
    var amount: Amount
}

struct Ingredients: Codable {
    var hops: [Hop]
    var malts: [Malt]

    enum CodingKeys: String, CodingKey {
        case malts = "malt"
        case hops = "hops"
    }
}

struct Method: Codable {
    var mash_temp: [MashTemp]
    var fermentation: Fermentation
    var twist: String?
}

struct Beer: Codable {
    var image_url: String?
    var name, description:  String
    var abv: Float
    var ibu: Float?
    var srm: Float?
    var ebc: Float?
    var ph: Float?
    var method: Method
    var ingredients: Ingredients
}
